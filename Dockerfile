FROM golang:1.17.2-alpine3.14

RUN mkdir ../server

WORKDIR /../server

COPY . .

RUN go build ./cmd/main.go

ENTRYPOINT [ "./main" ]

EXPOSE 8080