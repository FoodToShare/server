package models

import (
	"errors"
	"regexp"
)

type Beneficiary struct {
	Model
	FirstName       string  `gorm:"not null"  example:"Tim" json:"first_name"`
	LastName        string  `gorm:"not null"  example:"Berners-Lee" json:"last_name"`
	SocialSecurity  string  `gorm:"size:11; not null; unique"  example:"11506938276" json:"social_security"`
	CellPhone       string  `gorm:"size:9" example:"999555777" json:"cell_phone"`
	Address         string  `gorm:"not null"  example:"1600 Amphitheatre Parkway, Mountain View, CA, Postal Code 94043" json:"address"`
	DeliveryPointID *uint64 ` example:"2303829480249" json:"delivery_point_id"`
	FamilyGroup     uint64  `gorm:"default:2303829480777;not null" example:"2303829480249" json:"family_group"`
}

var (
	ErrBeneficiaryEmptyFirstName       = errors.New("beneficiary.firstName can't be empty")
	ErrBeneficiaryEmptyLastName        = errors.New("beneficiary.lastName can't be empty")
	ErrBeneficiaryEmptySocialSecurity  = errors.New("beneficiary.socialSecurity can't be empty")
	ErrBeneficiaryFormatSocialSecurity = errors.New("beneficiary.socialSecurity must be 11 digits")
)

func (b *Beneficiary) Validate() error {
	if b.FirstName == "" {
		return ErrBeneficiaryEmptyFirstName
	}
	if b.LastName == "" {
		return ErrBeneficiaryEmptyLastName
	}
	if b.SocialSecurity == "" {
		return ErrBeneficiaryEmptySocialSecurity
	}

	if matched, err := regexp.MatchString("^\\d{11}$", b.SocialSecurity); matched == false || err != nil {
		return ErrBeneficiaryFormatSocialSecurity
	}
	return nil
}
