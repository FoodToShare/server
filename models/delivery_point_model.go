package models

import (
	"errors"
	"fmt"
)

type DeliveryPoint struct {
	Model
	Name                     string        `gorm:"size:255;not null;unique"  example:"parish council" json:"name"`
	Description              string        `gorm:"size:512" example:"Needy Elderly people from the parish gather here." json:"description"`
	Latitude                 float64       `gorm:"type:decimal(17,14); not null"  example:"-8.846684428962709" json:"latitude"`
	Longitude                float64       `gorm:"type:decimal(17,14); not null"  example:"41.69330364869015" json:"longitude"`
	RouteID                  *uint64       `example:"2303829480249" json:"route_id"`
	FirstDelivery            bool          `gorm:"default:true" example:"true" json:"firstDelivery"`
	AddressCorrectionComment string        `example:"The address is 100m ahead towards the intersection." json:"addressCorrectionComment"`
	Beneficiaries            []Beneficiary `json:"beneficiaries"`
}

var (
	ErrDeliveryPointEmptyName = errors.New("deliveryPoint.name can't be empty")
)

func (dp *DeliveryPoint) Validate() error {
	if dp.Name == "" {
		return ErrDeliveryPointEmptyName
	}
	return nil
}

func (dp *DeliveryPoint) LatLongToString() string {
	return fmt.Sprintf("%.14F,%.14F;", dp.Latitude, dp.Longitude)
}
