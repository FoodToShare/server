package models

import (
	"time"
)

type Model struct {
	ID        uint64    `gorm:"primary_key;auto_increment" example:"2303829480249" json:"id"`
	CreatedAt time.Time `gorm:"<-:create;default:current_timestamp;not null" example:"2012-10-31 15:50:13.793654 +0000 UTC" json:"-"`
	UpdatedAt time.Time `gorm:"default:current_timestamp;not null" example:"2012-10-31 15:50:13.793654 +0000 UTC" json:"-"`
	Active    bool      `gorm:"default:true;not null" example:"true" json:"active"`
}
