package models

import (
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	Model
	Email          string   `gorm:"not null" json:"email" example:"admin@server.com"`
	Password       string   `gorm:"-" json:"password" example:"1234"`
	Role           UserRole `gorm:"not null" example:"VOLUNTARY" json:"role"`
	PasswordHashed string   `gorm:"not null" json:"-"`
}

func (u *User) BeforeUpdate(tx *gorm.DB) (err error) {
	if len(u.Password) > 0 {
		bytes, err := bcrypt.GenerateFromPassword([]byte(u.Password), 14)
		if err != nil {
			return err
		}
		u.PasswordHashed = string(bytes)
	}
	return nil
}
func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	if len(u.Password) > 0 {
		bytes, err := bcrypt.GenerateFromPassword([]byte(u.Password), 14)
		if err != nil {
			return err
		}
		u.PasswordHashed = string(bytes)
	}
	return nil
}

func (u *User) CheckPassword(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.PasswordHashed), []byte(password))
	return err == nil
}
