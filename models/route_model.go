package models

import (
	"errors"
)

type Route struct {
	Model
	Name           string          `gorm:"size:255;not null" example:"east of the city" json:"name"`
	Description    string          `gorm:"size:512" example:"route that circles the seafront promenade." json:"description"`
	DeliveryPoints []DeliveryPoint `json:"delivery_points"`
	UserID         *uint64         `json:"userID"`
	Responsible    User            `gorm:"foreignKey:UserID" json:"responsible"`
}

var (
	ErrRoutEmptyName = errors.New("route.name can't be empty")
)

func (r *Route) Validate() error {
	if r.Name == "" {
		return ErrRoutEmptyName
	}
	return nil
}
