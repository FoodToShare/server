package models

import "errors"

type DeliveryComments struct {
	Model
	BeneficiaryID uint64
	Beneficiary   Beneficiary `json:"beneficiary"`
	UserID        uint64      `json:"userID"`
	Author        User        `gorm:"foreignKey:UserID" json:"author"`
	Comment       string      `json:"comments"`
	Processed     bool        `gorm:"default:true" json:"processed"`
}

var (
	ErrEmptyComment = errors.New("DeliverComment.Comment can't be empty")
)

func (r *DeliveryComments) Validate() error {
	if r.Comment == "" {
		return ErrEmptyComment
	}
	return nil
}
