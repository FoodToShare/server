package models

type UserRole string

const (
	ADMIN     UserRole = "ADMIN"
	VOLUNTARY UserRole = "VOLUNTARY"
)
