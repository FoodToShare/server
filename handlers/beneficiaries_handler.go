package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"net/http"
	"strconv"
)

type BeneficiariesHandler interface {
	CreateBeneficiary(*gin.Context)
	DeleteBeneficiary(*gin.Context)
	SaveBeneficiary(*gin.Context)
	ShowBeneficiary(*gin.Context)
	ShowBeneficiaries(*gin.Context)
}

type beneficiariesHandlerImpl struct {
	beneficiariesRepository repositories.BeneficiariesRepository
}

func NewBeneficiaryHandler(beneficiariesRepository repositories.BeneficiariesRepository) *beneficiariesHandlerImpl {
	return &beneficiariesHandlerImpl{beneficiariesRepository}
}

// CreateBeneficiary godoc
// @Summary Create a beneficiary
// @Security bearerAuth
// @Description Create a beneficiary.
// @Tags Beneficiary
// @Accept json
// @Produce json
// @Param beneficiary body models.Beneficiary true "Beneficiary data in JSON"
// @Success 201 {object} models.Beneficiary "Return the beneficiary created."
// @Failure 400 {string} string "cannot bind JSON"
// @Router /beneficiary [post]
func (h *beneficiariesHandlerImpl) CreateBeneficiary(c *gin.Context) {
	var model models.Beneficiary

	if err := c.ShouldBindJSON(&model); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "cannot bind JSON:" + err.Error()})
		return
	}

	if err := model.Validate(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err := h.beneficiariesRepository.Save(&model)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, model)
}

// ShowBeneficiary godoc
// @Summary Show a beneficiary
// @Security bearerAuth
// @Description Get beneficiary by ID.
// @ID get-beneficiary-by-int
// @Tags Beneficiary
// @Param id path integer true "ID of beneficiary" minimum(1)
// @Accept json
// @Produce json
// @Success 200 {object} models.Beneficiary
// @Failure 400 body string "cannot bind JSON"
// @Router /beneficiary/{id} [get]
func (h *beneficiariesHandlerImpl) ShowBeneficiary(c *gin.Context) {
	id := c.Param("id")

	parseId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}

	var model models.Beneficiary
	model.ID = uint64(int64(parseId))

	_, err = h.beneficiariesRepository.FindByID(&model)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, model)
}

// DeleteBeneficiary godoc
// @Summary Delete a beneficiary
// @Security bearerAuth
// @Description Delete a beneficiary by ID.
// @ID delete-beneficiary-by-int
// @Tags Beneficiary
// @Param id path integer true "ID of beneficiary" minimum(1)
// @Accept json
// @Produce json
// @Success 200 {object} models.Beneficiary
// @Failure 400 body string "cannot bind JSON"
// @Router /beneficiary/{id} [delete]
func (h *beneficiariesHandlerImpl) DeleteBeneficiary(c *gin.Context) {
	id := c.Param("id")

	parseId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}

	var model models.Beneficiary
	model.ID = uint64(int64(parseId))

	_, err = h.beneficiariesRepository.FindByID(&model)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	_, err = h.beneficiariesRepository.Delete(&model)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, model)
}

// ShowBeneficiaries godoc
// @Summary Show all beneficiaries
// @Security bearerAuth
// @Schemes
// @Description Get all beneficiaries.
// @ID get-all-beneficiaries
// @Tags Beneficiary
// @Accept json
// @Produce json
// @Success 200 {array} models.Beneficiary
// @Failure 400 {string} string "error in database"
// @Router /beneficiary/all [get]
func (h *beneficiariesHandlerImpl) ShowBeneficiaries(c *gin.Context) {
	var model []models.Beneficiary

	_, err := h.beneficiariesRepository.FindAll(&model)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, model)
}

// SaveBeneficiary godoc
// @Summary Save a beneficiary
// @Security bearerAuth
// @Description Save a beneficiary.
// @Tags Beneficiary
// @Param id path integer true "ID of beneficiary" minimum(1)
// @Accept json
// @Produce json
// @Param beneficiary body models.Beneficiary true "Beneficiary data in JSON"
// @Success 200 {object} models.Beneficiary "Return the beneficiary saved."
// @Failure 400 {string} string "cannot bind JSON"
// @Router /beneficiary/{id} [patch]
func (h *beneficiariesHandlerImpl) SaveBeneficiary(c *gin.Context) {
	id := c.Param("id")

	parseId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}

	var model models.Beneficiary
	model.ID = uint64(int64(parseId))

	_, err = h.beneficiariesRepository.FindByID(&model)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := c.ShouldBindJSON(&model); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "cannot bind JSON:" + err.Error()})
		return
	}

	if err := model.Validate(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err = h.beneficiariesRepository.Save(&model)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, model)
}
