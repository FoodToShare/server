package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"gitlab.com/FoodToShare/server/services"
	"net/http"
	"strconv"
)

type RoutesHandler interface {
	CreateRoute(*gin.Context)
	DeleteRoute(*gin.Context)
	SaveRoute(*gin.Context)
	ShowRoutes(*gin.Context)
	ShowRoute(*gin.Context)
}

type routesHandlerImpl struct {
	routesRepository repositories.RoutesRepository
}

func NewRoutesHandler(routesRepository repositories.RoutesRepository) *routesHandlerImpl {
	return &routesHandlerImpl{routesRepository}
}

// ShowRoutes godoc
// @Summary Show all routes
// @Security bearerAuth
// @Description show all routes.
// @Tags Delivery
// @Produce json
// @Success 200 {array} models.Route "Return the routes."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /delivery/route [get]
func (h *routesHandlerImpl) ShowRoutes(c *gin.Context) {
	var model []models.Route

	_, err := h.routesRepository.FindAll(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, model)
}

// ShowRoute godoc
// @Summary Show a route
// @Security bearerAuth
// @Description Get route by ID.
// @Tags Delivery
// @Param id path integer true "ID of route" minimum(1)
// @Accept json
// @Produce json
// @Success 200 {object} models.Route "Return the route."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /delivery/route/{id} [get]
func (h *routesHandlerImpl) ShowRoute(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}
	var model models.Route
	model.ID = uint64(int64(id))

	if _, err := h.routesRepository.FindByID(&model); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	if err := services.OrderRouteByDistance(&model); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
	c.JSON(http.StatusOK, model)
}

// DeleteRoute godoc
// @Summary Delete a route
// @Security bearerAuth
// @Description Delete route by ID.
// @Tags Delivery
// @Param id path integer true "ID of route" minimum(1)
// @Accept json
// @Produce json
// @Success 200 {object} models.Route "Return the deleted route."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /delivery/route/{id} [delete]
func (h *routesHandlerImpl) DeleteRoute(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}
	var model models.Route
	model.ID = uint64(int64(id))

	if _, err := h.routesRepository.FindByID(&model); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	_, err = h.routesRepository.Delete(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, model)
}

// CreateRoute godoc
// @Summary Create a route
// @Security bearerAuth
// @Description Create a route.
// @Tags Delivery
// @Accept json
// @Produce json
// @Param beneficiary body models.Route true "Route data in JSON"
// @Success 201 {object} models.Route "Return created route."
// @Failure 400 {string} string "cannot bind JSON"
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /delivery/route [post]
func (h *routesHandlerImpl) CreateRoute(c *gin.Context) {
	var model models.Route

	if err := c.ShouldBindJSON(&model); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "cannot bind JSON:" + err.Error()})
		return
	}

	if err := model.Validate(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err := h.routesRepository.Save(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, model)
}

// SaveRoute godoc
// @Summary Save a route
// @Security bearerAuth
// @Description Save a route.
// @Tags Delivery
// @Param id path integer true "ID of route" minimum(1)
// @Accept json
// @Produce json
// @Param beneficiary body models.Route true "Route data in JSON"
// @Success 200 {object} models.Route "Return saved route."
// @Failure 400 {string} string "cannot bind JSON"
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /delivery/route/{id} [patch]
func (h *routesHandlerImpl) SaveRoute(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}
	var model models.Route
	model.ID = uint64(int64(id))

	if _, err := h.routesRepository.FindByID(&model); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	if err := c.ShouldBindJSON(&model); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "cannot bind JSON:" + err.Error()})
		return
	}

	if err := model.Validate(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err = h.routesRepository.Save(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, model)
}
