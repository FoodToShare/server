package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"net/http"
	"strconv"
)

type UsersHandler interface {
	CreateUser(*gin.Context)
	DeleteUser(*gin.Context)
	ShowUser(*gin.Context)
	ShowUsers(*gin.Context)
	SaveUser(*gin.Context)
}

type usersHandlerImpl struct {
	usersRepository repositories.UsersRepository
}

func NewUsersHandler(usersRepository repositories.UsersRepository) *usersHandlerImpl {
	return &usersHandlerImpl{usersRepository}
}

// ShowUsers godoc
// @Summary Show all users
// @Security bearerAuth
// @Description show all users.
// @Tags User
// @Produce json
// @Success 200 {array} models.User "Return the users."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /user/ [get]
func (h *usersHandlerImpl) ShowUsers(c *gin.Context) {
	var model []models.User

	_, err := h.usersRepository.FindAll(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, model)
}

// ShowUser godoc
// @Summary Show a user
// @Security bearerAuth
// @Description Get user by ID.
// @Tags User
// @Param id path integer true "ID of user" minimum(1)
// @Accept json
// @Produce json
// @Success 200 {object} models.User "Return the user."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /user/{id} [get]
func (h *usersHandlerImpl) ShowUser(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}
	var model models.User
	model.ID = uint64(int64(id))

	if _, err := h.usersRepository.FindByID(&model); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, model)
}

// DeleteUser godoc
// @Summary Delete a user
// @Security bearerAuth
// @Description Delete user by ID.
// @Tags User
// @Param id path integer true "ID of user" minimum(1)
// @Accept json
// @Produce json
// @Success 200 {object} models.User "Return the deleted user."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /user/{id} [delete]
func (h *usersHandlerImpl) DeleteUser(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}
	var model models.User
	model.ID = uint64(int64(id))

	if _, err := h.usersRepository.FindByID(&model); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	_, err = h.usersRepository.Delete(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, model)
}

// CreateUser godoc
// @Summary Create a user
// @Security bearerAuth
// @Description Create a user.
// @Tags User
// @Accept json
// @Produce json
// @Param beneficiary body models.User true "User data in JSON"
// @Success 201 {object} models.User "Return created user."
// @Failure 400 {string} string "cannot bind JSON"
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /user/ [post]
func (h *usersHandlerImpl) CreateUser(c *gin.Context) {
	var model models.User

	if err := c.ShouldBindJSON(&model); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "cannot bind JSON:" + err.Error()})
		return
	}

	_, err := h.usersRepository.Save(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, model)
}

// SaveUser godoc
// @Summary Save a user
// @Security bearerAuth
// @Description Save a user.
// @Tags User
// @Param id path integer true "ID of user" minimum(1)
// @Accept json
// @Produce json
// @Param user body models.User true "User data in JSON"
// @Success 200 {object} models.User "Return saved user."
// @Failure 400 {string} string "cannot bind JSON"
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /user/{id} [patch]
func (h *usersHandlerImpl) SaveUser(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}
	var model models.User
	model.ID = uint64(int64(id))

	if _, err := h.usersRepository.FindByID(&model); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	if err := c.ShouldBindJSON(&model); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "cannot bind JSON:" + err.Error()})
		return
	}

	_, err = h.usersRepository.Save(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, model)
}
