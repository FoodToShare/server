package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/FoodToShare/server/auth"
	"gitlab.com/FoodToShare/server/models"
	"net/http"
)

// Login godoc
// @Summary Provides a JSON Web Token
// @Description Authenticates a user and provides a JWT to Authorize API calls.
// @ID Authentication
// @Tags Auth
// @Consume application/json
// @Param credentials body models.User true "User credentials"
// @Produce json
// @Success 200 {object} models.TokenCombo "Return an accessToken = JWT and refreshToken = JWT"
// @Failure 401 {string} string "Please provide valid login details"
// @Failure 422 {string} string "Invalid json provided"
// @Router /auth/login [post]
func Login(c *gin.Context) {
	var u models.User
	if err := c.ShouldBindJSON(&u); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "Invalid json provided")
		return
	}

	token, err := auth.Login(u.Email, u.Password)

	if err != nil {
		switch err.Error() {
		case auth.ERROR_USER_NOT_FOUND:
			c.JSON(http.StatusUnauthorized, "Please provide valid login details")
			return
		case auth.INCORRECT_PASSWORD:
			c.JSON(http.StatusUnauthorized, "Please provide valid login details")
			return
		case auth.ERROR_IN_TOKEN_CREATION:
			c.JSON(http.StatusUnprocessableEntity, err.Error())
			return
		case auth.ERROR_SAVING_TOKEN:
			c.JSON(http.StatusUnprocessableEntity, err.Error())
			return
		}
	}

	tokenCombo := models.TokenCombo{
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
	}

	c.JSON(http.StatusOK, tokenCombo)
}

// Logout godoc
// @Summary User login
// @Security bearerAuth
// @Description Do user login.
// @Tags Auth
// @Accept json
// @Produce json
// @Success 200 {string} string "Successfully logged out"
// @Failure 401 {string} string "unauthorized"
// @Router /auth/logout [post]
func Logout(c *gin.Context) {
	err := auth.Logout(c.Request)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	c.JSON(http.StatusOK, "Successfully logged out")
}
