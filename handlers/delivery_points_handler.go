package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"net/http"
	"strconv"
)

type DeliveryPointsHandler interface {
	CreateDeliveryPoint(*gin.Context)
	DeleteDeliveryPoint(*gin.Context)
	ShowDeliveryPoints(*gin.Context)
	ShowDeliveryPoint(*gin.Context)
	ShowDeliveryPointsByRoute(*gin.Context)
}

type deliveryPointsHandlerImpl struct {
	deliveryPointRepository repositories.DeliveryPointsRepository
}

func NewDeliveryPointsHandler(deliveryPointRepository repositories.DeliveryPointsRepository) *deliveryPointsHandlerImpl {
	return &deliveryPointsHandlerImpl{deliveryPointRepository}
}

// ShowDeliveryPoints godoc
// @Summary Show all Delivery Points
// @Security bearerAuth
// @Description show all Delivery Points.
// @Tags Delivery
// @Produce json
// @Success 200 {array} models.DeliveryPoint "Return the Delivery Points."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /delivery/point [get]
func (h *deliveryPointsHandlerImpl) ShowDeliveryPoints(c *gin.Context) {
	var model []models.DeliveryPoint

	_, err := h.deliveryPointRepository.FindAll(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, model)
}

// ShowDeliveryPoint godoc
// @Summary Show a Delivery Point
// @Security bearerAuth
// @Description Get Delivery Point by ID.
// @Tags Delivery
// @Param id path integer true "ID of Delivery Point" minimum(1)
// @Accept json
// @Produce json
// @Success 200 {object} models.DeliveryPoint "Return the Delivery Point."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /delivery/point/{id} [get]
func (h *deliveryPointsHandlerImpl) ShowDeliveryPoint(c *gin.Context) {
	id := c.Param("id")
	parseId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}
	var model models.DeliveryPoint
	model.ID = uint64(int64(parseId))

	_, err = h.deliveryPointRepository.FindByID(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, model)
}

// DeleteDeliveryPoint godoc
// @Summary Delete a Delivery Point
// @Security bearerAuth
// @Description Delete Delivery Point by ID.
// @Tags Delivery
// @Param id path integer true "ID of Delivery Point" minimum(1)
// @Accept json
// @Produce json
// @Success 200 {object} models.DeliveryPoint "Return the Delivery Point."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /delivery/point/{id} [delete]
func (h *deliveryPointsHandlerImpl) DeleteDeliveryPoint(c *gin.Context) {
	id := c.Param("id")
	parseId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}
	var model models.DeliveryPoint
	model.ID = uint64(int64(parseId))

	_, err = h.deliveryPointRepository.FindByID(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
	_, err = h.deliveryPointRepository.Delete(&model)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, model)
}

// CreateDeliveryPoint godoc
// @Summary Create a Delivery Point
// @Security bearerAuth
// @Description Create a Delivery Point.
// @Tags Delivery
// @Accept json
// @Produce json
// @Param DeliveryPoint body models.DeliveryPoint true "Delivery Point data in JSON"
// @Success 201 {object} models.DeliveryPoint "Return created Delivery Point."
// @Failure 400 {string} string "cannot bind JSON"
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /delivery/point [post]
func (h *deliveryPointsHandlerImpl) CreateDeliveryPoint(c *gin.Context) {
	var model models.DeliveryPoint

	if err := c.ShouldBindJSON(&model); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "cannot bind JSON:" + err.Error()})
		return
	}

	if err := model.Validate(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err := h.deliveryPointRepository.Save(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, model)
}

// SaveDeliveryPoint godoc
// @Summary Save a Delivery Point
// @Security bearerAuth
// @Description Save a Delivery Point.
// @Tags Delivery
// @Param id path integer true "ID of Delivery Point" minimum(1)
// @Accept json
// @Produce json
// @Param DeliveryPoint body models.DeliveryPoint true "Delivery Point data in JSON"
// @Success 200 {object} models.DeliveryPoint "Return saved Delivery Point."
// @Failure 400 {string} string "cannot bind JSON"
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /delivery/point/{id} [patch]
func (h *deliveryPointsHandlerImpl) SaveDeliveryPoint(c *gin.Context) {
	id := c.Param("id")
	parseId, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}
	var model models.DeliveryPoint
	model.ID = uint64(int64(parseId))

	_, err = h.deliveryPointRepository.FindByID(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	if err := c.ShouldBindJSON(&model); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "cannot bind JSON:" + err.Error()})
		return
	}

	_, err = h.deliveryPointRepository.Save(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, model)
}
