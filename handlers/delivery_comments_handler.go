package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"net/http"
	"strconv"
)

type DeliveryCommentsHandler interface {
	CreateDeliveryComments(*gin.Context)
	ShowDeliveryComments(*gin.Context)
	ShowDeliveryComment(*gin.Context)
}

type deliveryCommentsHandlerImpl struct {
	deliveryCommentsRepository repositories.DeliveryCommentsRepository
}

func NewDeliveryCommentsHandler(deliveryCommentsRepository repositories.DeliveryCommentsRepository) *deliveryCommentsHandlerImpl {
	return &deliveryCommentsHandlerImpl{deliveryCommentsRepository}
}

// ShowDeliveryComments godoc
// @Summary Show all delivery comments
// @Security bearerAuth
// @Description show all delivery comments.
// @Tags Comments
// @Produce json
// @Success 200 {array} models.DeliveryComments "Return the delivery comments."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /comment/delivery/ [get]
func (h *deliveryCommentsHandlerImpl) ShowDeliveryComments(c *gin.Context) {
	var model []models.DeliveryComments

	_, err := h.deliveryCommentsRepository.FindAll(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, model)
}

// ShowDeliveryComment godoc
// @Summary Show a delivery comment
// @Security bearerAuth
// @Description Get delivery comment by ID.
// @Tags Comments
// @Param id path integer true "ID of delivery comment" minimum(1)
// @Accept json
// @Produce json
// @Success 200 {object} models.DeliveryComments "Return the delivery comment."
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /comment/delivery/{id} [get]
func (h *deliveryCommentsHandlerImpl) ShowDeliveryComment(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID has to be integer"})
		return
	}
	var model models.DeliveryComments
	model.ID = uint64(int64(id))

	if _, err := h.deliveryCommentsRepository.FindByID(&model); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, model)
}

// CreateDeliveryComment godoc
// @Summary Create a delivery comment
// @Security bearerAuth
// @Description Create a delivery comment.
// @Tags Comments
// @Accept json
// @Produce json
// @Param beneficiary body models.DeliveryComments true "delivery comment data in JSON"
// @Success 201 {object} models.DeliveryComments "Return created delivery comment."
// @Failure 400 {string} string "cannot bind JSON"
// @Failure 500 {string} string "Internal Server Error in Data Base"
// @Router /comment/delivery/ [post]
func (h *deliveryCommentsHandlerImpl) CreateDeliveryComment(c *gin.Context) {
	var model models.DeliveryComments

	if err := c.ShouldBindJSON(&model); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "cannot bind JSON:" + err.Error()})
		return
	}

	if err := model.Validate(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err := h.deliveryCommentsRepository.Save(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, model)
}
