package routes

import (
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"gitlab.com/FoodToShare/server/auth"
	"gitlab.com/FoodToShare/server/databases"
	"gitlab.com/FoodToShare/server/handlers"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"net/http"
)

func ConfigRoutes(server *gin.Engine) {

	mainGroup := server.Group("api/v1")
	{
		swagger := mainGroup.Group("swagger")
		{
			swagger.GET("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
		}
		userGroup := mainGroup.Group("user")
		{
			userHandler := handlers.NewUsersHandler(repositories.NewUsersRepository(databases.GetDatabase()))
			userGroup.GET("/:id", TokenAuthMiddleware(), userHandler.ShowUser)
			userGroup.DELETE("/:id", TokenAuthMiddleware(), OnlyAdmin(), userHandler.DeleteUser)
			userGroup.GET("/", TokenAuthMiddleware(), userHandler.ShowUsers)
			userGroup.POST("/", TokenAuthMiddleware(), OnlyAdmin(), userHandler.CreateUser)
			userGroup.PATCH("/:id", TokenAuthMiddleware(), OnlyAdmin(), userHandler.SaveUser)
		}
		loginGroup := mainGroup.Group("auth")
		{
			loginGroup.POST("/login", handlers.Login)
			loginGroup.POST("/logout", TokenAuthMiddleware(), handlers.Logout)
		}
		beneficiaryGroup := mainGroup.Group("beneficiary")
		{
			BeneficiaryHandler := handlers.NewBeneficiaryHandler(repositories.NewBeneficiariesRepository(databases.GetDatabase()))
			beneficiaryGroup.GET("/:id", TokenAuthMiddleware(), BeneficiaryHandler.ShowBeneficiary)
			beneficiaryGroup.DELETE("/:id", TokenAuthMiddleware(), OnlyAdmin(), BeneficiaryHandler.DeleteBeneficiary)
			beneficiaryGroup.GET("/all", TokenAuthMiddleware(), BeneficiaryHandler.ShowBeneficiaries)
			beneficiaryGroup.POST("/", TokenAuthMiddleware(), OnlyAdmin(), BeneficiaryHandler.CreateBeneficiary)
			beneficiaryGroup.PATCH("/:id", TokenAuthMiddleware(), OnlyAdmin(), BeneficiaryHandler.SaveBeneficiary)
		}
		commentsGroup := mainGroup.Group("comment")
		{
			commentDeliveryGroup := commentsGroup.Group("delivery")
			{
				commentsHandler := handlers.NewDeliveryCommentsHandler(repositories.NewDeliveryCommentsRepository(databases.GetDatabase()))
				commentDeliveryGroup.GET("/", TokenAuthMiddleware(), commentsHandler.ShowDeliveryComments)
				commentDeliveryGroup.GET("/:id", TokenAuthMiddleware(), commentsHandler.ShowDeliveryComment)
				commentDeliveryGroup.POST("/", TokenAuthMiddleware(), OnlyAdmin(), commentsHandler.CreateDeliveryComment)
			}
		}
		deliveryGroup := mainGroup.Group("delivery")
		{
			routesGroup := deliveryGroup.Group("route")
			{
				routesHandler := handlers.NewRoutesHandler(repositories.NewRoutesRepository(databases.GetDatabase()))
				routesGroup.GET("/", TokenAuthMiddleware(), routesHandler.ShowRoutes)
				routesGroup.GET("/:id", TokenAuthMiddleware(), routesHandler.ShowRoutes)
				routesGroup.DELETE("/:id", TokenAuthMiddleware(), OnlyAdmin(), routesHandler.DeleteRoute)
				routesGroup.POST("/", TokenAuthMiddleware(), OnlyAdmin(), routesHandler.CreateRoute)
				routesGroup.PATCH("/:id", TokenAuthMiddleware(), OnlyAdmin(), routesHandler.SaveRoute)
			}
			deliveryPointsGroup := deliveryGroup.Group("point")
			{
				pointsHandler := handlers.NewDeliveryPointsHandler(repositories.NewDeliveryPointRepository(databases.GetDatabase()))
				deliveryPointsGroup.GET("/", TokenAuthMiddleware(), pointsHandler.ShowDeliveryPoints)
				deliveryPointsGroup.GET("/:id", TokenAuthMiddleware(), pointsHandler.ShowDeliveryPoint)
				deliveryPointsGroup.DELETE("/:id", TokenAuthMiddleware(), OnlyAdmin(), pointsHandler.DeleteDeliveryPoint)
				deliveryPointsGroup.POST("/", TokenAuthMiddleware(), OnlyAdmin(), pointsHandler.CreateDeliveryPoint)
				deliveryPointsGroup.PATCH("/:id", TokenAuthMiddleware(), OnlyAdmin(), pointsHandler.SaveDeliveryPoint)
			}
		}
	}
}

func TokenAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := auth.TokenAuthMiddleware(c.Request)
		if err != nil {
			c.JSON(http.StatusUnauthorized, "unauthorized")
			c.Abort()
			return
		}
		c.Next()
	}
}

func OnlyAdmin() gin.HandlerFunc {
	return func(c *gin.Context) {
		accessDetails, err := auth.ExtractTokenMetadata(c.Request)
		if err != nil {
			c.JSON(http.StatusUnauthorized, "unauthorized")
			c.Abort()
			return
		}
		user := &models.User{
			Model: models.Model{
				ID: accessDetails.UserId,
			},
		}
		_, err = repositories.NewUsersRepository(databases.GetDatabase()).FindByID(user)
		if user.Role != models.ADMIN {
			c.JSON(http.StatusForbidden, "Only Admin")
			c.Abort()
			return
		}
		c.Next()
	}
}
