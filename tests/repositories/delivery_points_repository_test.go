package repositories

import (
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"regexp"
	"testing"
)

type SuitePoint struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.DeliveryPointsRepository
	model      *models.DeliveryPoint
}

func (s *SuitePoint) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	dialect := postgres.New(postgres.Config{
		DSN:                  "sqlmock_db_0",
		DriverName:           "postgres",
		Conn:                 db,
		PreferSimpleProtocol: true,
	})
	s.DB, err = gorm.Open(dialect, &gorm.Config{SkipDefaultTransaction: true})
	require.NoError(s.T(), err)

	s.repository = repositories.NewDeliveryPointRepository(s.DB)
}

func (s *SuitePoint) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestInitPoint(t *testing.T) {
	suite.Run(t, new(SuitePoint))
}

func (s *SuitePoint) TestSave() {
	var routeID uint64 = 4183870180679
	model := models.DeliveryPoint{
		Model:                    models.Model{},
		Name:                     "parish council",
		Description:              "Needy Elderly people from the parish gather here.",
		Latitude:                 -8.83016,
		Longitude:                41.6946,
		RouteID:                  &routeID,
		FirstDelivery:            false,
		AddressCorrectionComment: "",
	}
	s.mock.ExpectQuery(regexp.QuoteMeta(`SELECT count(*) FROM "delivery_points" WHERE active = $1 AND id = $2`)).
		WithArgs(true, model.ID).WillReturnRows(sqlmock.NewRows([]string{"count(*)"}).AddRow(0))
	s.mock.ExpectBegin()

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "delivery_points" ("active","name","description","latitude","longitude","route_id","first_delivery","address_correction_comment") 
			VALUES ($1,$2,$3,$4,$5,$6,$7,$8)`)).
		WithArgs(true, model.Name, model.Description, model.Latitude, model.Longitude, model.RouteID, true, model.AddressCorrectionComment).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "created_at", "updated_at"}))
	s.mock.ExpectCommit()

	_, err := s.repository.Save(&model)

	require.NoError(s.T(), err)
}
func (s *SuitePoint) TestFindAll() {
	var model []models.DeliveryPoint

	s.mock.ExpectQuery(regexp.QuoteMeta(`SELECT * FROM "delivery_points" WHERE active = $1`)).
		WithArgs(true).WillReturnRows(sqlmock.NewRows([]string{"id", "name", "active"}))

	_, err := s.repository.FindAll(&model)
	require.NoError(s.T(), err)
}

func (s *SuitePoint) TestFindById() {
	model := models.DeliveryPoint{
		Model: models.Model{ID: 1},
	}
	s.mock.ExpectQuery(regexp.QuoteMeta(`SELECT * FROM "delivery_points" WHERE active = $1 AND "delivery_points"."id" = $2`)).
		WithArgs(true, model.ID).
		WillReturnRows(sqlmock.NewRows([]string{"id", "name", "active"}).AddRow(nil, nil, nil))

	_, err := s.repository.FindByID(&model)
	require.NoError(s.T(), err)
}
