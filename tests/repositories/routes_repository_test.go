package repositories

import (
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"regexp"
	"testing"
)

type SuiteRoute struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.RoutesRepository
	model      *models.Route
}

func (s *SuiteRoute) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	dialect := postgres.New(postgres.Config{
		DSN:                  "sqlmock_db_0",
		DriverName:           "postgres",
		Conn:                 db,
		PreferSimpleProtocol: true,
	})
	s.DB, err = gorm.Open(dialect, &gorm.Config{SkipDefaultTransaction: true})
	require.NoError(s.T(), err)

	//s.DB.LogMode(true)

	s.repository = repositories.NewRoutesRepository(s.DB)
}

func (s *SuiteRoute) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestInitRoute(t *testing.T) {
	suite.Run(t, new(SuiteRoute))
}

func (s *SuiteRoute) TestSave() {
	var userID uint64 = 1
	route := models.Route{
		Name:           "east of the city",
		Description:    "route that circles the seafront promenade.",
		DeliveryPoints: nil,
		UserID:         &userID,
	}
	s.mock.ExpectQuery(regexp.QuoteMeta(`SELECT count(*) FROM "routes" WHERE active = $1 AND id = $2`)).
		WithArgs(true, route.ID).WillReturnRows(sqlmock.NewRows([]string{"count(*)"}).AddRow(0))
	s.mock.ExpectBegin()

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "routes" ("active","name","description","user_id") 
			VALUES ($1,$2,$3,$4)`)).
		WithArgs(true, route.Name, route.Description, route.UserID).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "created_at", "updated_at"}))
	s.mock.ExpectCommit()

	_, err := s.repository.Save(&route)

	require.NoError(s.T(), err)
}

func (s *SuiteRoute) TestFindAll() {
	var route []models.Route

	s.mock.ExpectQuery(regexp.QuoteMeta(`SELECT * FROM "routes" WHERE active = $1`)).
		WithArgs(true).WillReturnRows(sqlmock.NewRows([]string{"id", "name", "active"}).AddRow(nil, nil, nil))

	_, err := s.repository.FindAll(&route)
	require.NoError(s.T(), err)
}

func (s *SuiteRoute) TestFindById() {
	route := models.Route{
		Model: models.Model{ID: 1},
	}
	s.mock.ExpectQuery(regexp.QuoteMeta(`SELECT * FROM "routes" WHERE active = $1 AND "routes"."id" = $2`)).
		WithArgs(true, route.ID).
		WillReturnRows(sqlmock.NewRows([]string{"id", "name", "active"}).AddRow(nil, nil, nil))

	_, err := s.repository.FindByID(&route)
	require.NoError(s.T(), err)
}
