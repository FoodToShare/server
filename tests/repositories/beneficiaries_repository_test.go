package repositories

import (
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"regexp"
	"testing"
)

type SuiteBeneficiary struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.BeneficiariesRepository
	model      *models.Beneficiary
}

func (s *SuiteBeneficiary) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	dialect := postgres.New(postgres.Config{
		DSN:                  "sqlmock_db_0",
		DriverName:           "postgres",
		Conn:                 db,
		PreferSimpleProtocol: true,
	})
	s.DB, err = gorm.Open(dialect, &gorm.Config{SkipDefaultTransaction: true})
	require.NoError(s.T(), err)

	s.repository = repositories.NewBeneficiariesRepository(s.DB)
}

func (s *SuiteBeneficiary) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestInitBeneficiary(t *testing.T) {
	suite.Run(t, new(SuiteBeneficiary))
}

func (s *SuiteBeneficiary) TestSave() {
	var pointID uint64 = 2303829480249
	model := models.Beneficiary{
		Model:           models.Model{},
		FirstName:       "Tim",
		LastName:        "Berners-Lee",
		SocialSecurity:  "11506938276",
		Address:         "1600 Amphitheatre Parkway, Mountain View, CA, Postal Code 94043",
		DeliveryPointID: &pointID,
	}
	s.mock.ExpectQuery(regexp.QuoteMeta(`SELECT count(*) FROM "beneficiaries" WHERE active = $1 AND id = $2`)).
		WithArgs(true, model.ID).WillReturnRows(sqlmock.NewRows([]string{"count(*)"}).AddRow(0))
	s.mock.ExpectBegin()

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "beneficiaries"`)).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "created_at", "updated_at"}))
	s.mock.ExpectCommit()

	_, err := s.repository.Save(&model)

	require.NoError(s.T(), err)
}
func (s *SuiteBeneficiary) TestFindAll() {
	var model []models.Beneficiary

	s.mock.ExpectQuery(regexp.QuoteMeta(`SELECT * FROM "beneficiaries" WHERE active = $1`)).
		WithArgs(true).WillReturnRows(sqlmock.NewRows([]string{"id", "name", "active"}))

	_, err := s.repository.FindAll(&model)
	require.NoError(s.T(), err)
}

func (s *SuiteBeneficiary) TestFindById() {
	model := models.Beneficiary{
		Model: models.Model{ID: 1},
	}
	s.mock.ExpectQuery(regexp.QuoteMeta(`SELECT * FROM "beneficiaries" WHERE active = $1 AND "beneficiaries"."id" = $2`)).
		WithArgs(true, model.ID).
		WillReturnRows(sqlmock.NewRows([]string{"id", "name", "active"}).AddRow(nil, nil, nil))

	_, err := s.repository.FindByID(&model)
	require.NoError(s.T(), err)
}
