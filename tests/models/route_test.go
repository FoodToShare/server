package models

import (
	"gitlab.com/FoodToShare/server/models"
	"testing"
)

func TestRouteValidate(t *testing.T) {
	route := &models.Route{}

	if err := route.Validate(); err != models.ErrRoutEmptyName {
		t.Error(err)
	}

	route.Name = "east of the city"

	if err := route.Validate(); err != nil {
		t.Error(err)
	}
}
