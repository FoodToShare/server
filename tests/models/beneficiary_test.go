package models

import (
	"gitlab.com/FoodToShare/server/models"
	"testing"
)

func TestBeneficiaryValidate(t *testing.T) {
	beneficiary := getBeneficiary()

	if err := beneficiary.Validate(); err != nil {
		t.Error(err)
	}

	beneficiary.FirstName = ""
	if err := beneficiary.Validate(); err != models.ErrBeneficiaryEmptyFirstName {
		t.Error(err)
	}

	beneficiary = getBeneficiary()
	beneficiary.LastName = ""
	if err := beneficiary.Validate(); err != models.ErrBeneficiaryEmptyLastName {
		t.Error(err)
	}

	beneficiary = getBeneficiary()
	beneficiary.SocialSecurity = ""
	if err := beneficiary.Validate(); err != models.ErrBeneficiaryEmptySocialSecurity {
		t.Error(err)
	}

	beneficiary = getBeneficiary()
	beneficiary.SocialSecurity = "115069382762"
	if err := beneficiary.Validate(); err != models.ErrBeneficiaryFormatSocialSecurity {
		t.Error(err)
	}

	beneficiary = getBeneficiary()
	beneficiary.SocialSecurity = "1150693827"
	if err := beneficiary.Validate(); err != models.ErrBeneficiaryFormatSocialSecurity {
		t.Error(err)
	}

	beneficiary = getBeneficiary()
	beneficiary.SocialSecurity = "11506f38276"
	if err := beneficiary.Validate(); err != models.ErrBeneficiaryFormatSocialSecurity {
		t.Error(err)
	}
}

func getBeneficiary() *models.Beneficiary {
	return &models.Beneficiary{
		Model:          models.Model{},
		FirstName:      "Tim",
		LastName:       "Berners-Lee",
		SocialSecurity: "11506938276",
		CellPhone:      "999555777",
		Address:        "",
	}
}
