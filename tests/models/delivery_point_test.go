package models

import (
	"gitlab.com/FoodToShare/server/models"
	"testing"
	"time"
)

func TestDeliveryPointValidate(t *testing.T) {
	dp := &models.DeliveryPoint{}

	if err := dp.Validate(); err != models.ErrDeliveryPointEmptyName {
		t.Error(err)
	}

	dp.Name = "parish council"
	if err := dp.Validate(); err != nil {
		t.Error(err)
	}
}

func TestCoordinatesToString(t *testing.T) {
	var routeID uint64 = 4183870180679
	dp := &models.DeliveryPoint{
		Model: models.Model{
			ID:        2303829480249,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Active:    true,
		},
		Name:          "parish council",
		Description:   "Needy Elderly people from the parish gather here.",
		Latitude:      -8.830238444776342,
		Longitude:     41.69469148320722,
		RouteID:       &routeID,
		Beneficiaries: nil,
	}
	coordinates := dp.LatLongToString()
	expected := "-8.83023844477634,41.69469148320722;"
	if coordinates != expected {
		t.Errorf("method returned '%s' when expected was '%s'", coordinates, expected)
	}
}
