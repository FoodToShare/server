package services

import (
	"log"
	"os"
	"testing"
	"time"

	"github.com/joho/godotenv"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/services"
)

func bootEnvironment() error {
	var err error

	if os.Getenv("ENV") == "test" {
		err = godotenv.Load("../../.env.test")
	} else {
		err = godotenv.Load("../../.env") //Loads the default file
	}

	if err != nil {
		log.Print("WARNING: Error loading .env file") //It's a warning because the variables could be loaded already
	}
	return nil
}

func getRoute() models.Route {
	r := models.Route{
		Model: models.Model{
			ID:        4183870180679,
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
			Active:    true,
		},
		Name:           "east of the city",
		Description:    "route that circles the seafront promenade.",
		DeliveryPoints: make([]models.DeliveryPoint, 0),
	}
	var routeID uint64 = 4183870180679
	r.DeliveryPoints = append(r.DeliveryPoints, models.DeliveryPoint{
		Model: models.Model{
			ID:     3, //UMINHO
			Active: true,
		},
		Name:      "DP 1",
		Latitude:  -8.396666523515954,
		Longitude: 41.56005895821732,
		RouteID:   &routeID,
	})
	r.DeliveryPoints = append(r.DeliveryPoints, models.DeliveryPoint{
		Model: models.Model{
			ID:     1,
			Active: true,
		},
		Name:      "DP 1",
		Latitude:  -8.830238444776342,
		Longitude: 41.69469148320722,
		RouteID:   &routeID,
	})
	r.DeliveryPoints = append(r.DeliveryPoints, models.DeliveryPoint{
		Model: models.Model{
			ID:     2, //IPCA
			Active: true,
		},
		Name:      "DP 1",
		Latitude:  -8.62773175539416,
		Longitude: 41.537046655376585,
		RouteID:   &routeID,
	})

	return r
}

func TestOrderRoute(t *testing.T) {
	if err := bootEnvironment(); err != nil {
		t.Error(err)
	}
	var uminho uint64 = 3
	var viana uint64 = 1
	var ipca uint64 = 2
	route := getRoute()
	if route.DeliveryPoints[0].ID != uminho {
		t.Errorf("method returned pré-sort id = '%d'  when expected was '%d'", route.DeliveryPoints[0].ID, uminho)
	}
	if route.DeliveryPoints[1].ID != viana {
		t.Errorf("method returned pré-sort id = '%d'  when expected was '%d'", route.DeliveryPoints[1].ID, viana)
	}
	if route.DeliveryPoints[2].ID != ipca {
		t.Errorf("method returned pré-sort id = '%d'  when expected was '%d'", route.DeliveryPoints[2].ID, ipca)
	}

	if err := services.OrderRouteByDistance(&route); err != nil {
		t.Error(err)
	}

	if route.DeliveryPoints[0].ID != viana {
		t.Errorf("method returned id = '%d'  when expected was '%d'", route.DeliveryPoints[0].ID, viana)
	}
	if route.DeliveryPoints[1].ID != ipca {
		t.Errorf("method returned id = '%d'  when expected was '%d'", route.DeliveryPoints[1].ID, ipca)
	}
	if route.DeliveryPoints[2].ID != uminho {
		t.Errorf("method returned id = '%d'  when expected was '%d'", route.DeliveryPoints[2].ID, uminho)
	}
}
