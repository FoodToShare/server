package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/FoodToShare/server/databases"
	"gitlab.com/FoodToShare/server/docs"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"gitlab.com/FoodToShare/server/servers"
	"log"
	"os"
)

// @title Food to Share API
// @version 1.0
// @description Main API of the FOOD TO SHARE application.

// @securityDefinitions.apikey bearerAuth
// @in header
// @name Authorization

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @contact.name API Support
// @contact.url https://www.fts.com/support
// @contact.email support@fts.com

// @termsOfService https://www.fts.com/terms/

// @schemes http

func main() {

	err := godotenv.Load(".env")
	if err != nil {
		log.Print("WARNING: Error loading .env file")
	}

	apiVersion := os.Getenv("API_VERSION")
	if len(apiVersion) == 0 {
		apiVersion = "1.0.0"
	}

	apiBasePath := os.Getenv("API_BASE_PATH")
	if len(apiBasePath) == 0 {
		apiBasePath = "api/v1"
	}

	apiHost := os.Getenv("API_HOST")
	if len(apiHost) == 0 {
		apiHost = "localhost:8080/"
	}

	docs.SwaggerInfo.Version = apiVersion
	docs.SwaggerInfo.Host = apiHost
	docs.SwaggerInfo.BasePath = apiBasePath

	userRepository := repositories.NewUsersRepository(databases.GetDatabase())
	dest := &[]models.User{}
	_, err = userRepository.FindAll(dest)
	if err != nil {
		return
	}

	if len(*dest) == 0 {
		user := &models.User{
			Model: models.Model{
				Active: true,
			},
			Role:     models.ADMIN,
			Email:    "admin@server.com",
			Password: "1234",
		}
		_, err := userRepository.Save(user)
		if err != nil {
			return
		}
	}

	servers.Start()
}
