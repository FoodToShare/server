package repositories

import (
	"gitlab.com/FoodToShare/server/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type BeneficiariesRepository interface {
	Save(*models.Beneficiary) (*models.Beneficiary, error)
	Delete(*models.Beneficiary) (*models.Beneficiary, error)
	FindAll(*[]models.Beneficiary) (*[]models.Beneficiary, error)
	FindByID(*models.Beneficiary) (*models.Beneficiary, error)
}

type beneficiariesRepositoryImpl struct {
	db *gorm.DB
}

func NewBeneficiariesRepository(db *gorm.DB) *beneficiariesRepositoryImpl {
	return &beneficiariesRepositoryImpl{db}
}

func (r *beneficiariesRepositoryImpl) Save(model *models.Beneficiary) (*models.Beneficiary, error) {
	var totalRecords int64
	var err error
	r.db.Model(&models.Beneficiary{}).Where(ACTIVE, true).Where("id = ?", model.ID).Count(&totalRecords)
	tx := r.db.Begin()

	if totalRecords == 0 {
		err = tx.Debug().Model(&models.Beneficiary{}).Create(model).Error
	} else {
		err = tx.Debug().Model(model).Omit(clause.Associations).Model(model).Select("*").Updates(model).Error
	}
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	return model, tx.Commit().Error
}
func (r *beneficiariesRepositoryImpl) Delete(model *models.Beneficiary) (*models.Beneficiary, error) {
	err := r.db.Model(model).Update("active", "false").Error
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (r *beneficiariesRepositoryImpl) FindAll(dest *[]models.Beneficiary) (*[]models.Beneficiary, error) {
	err := r.db.Where(ACTIVE, true).Find(dest).Error
	if err != nil {
		return dest, nil
	}
	return dest, err
}

func (r *beneficiariesRepositoryImpl) FindByID(model *models.Beneficiary) (*models.Beneficiary, error) {
	err := r.db.Where(ACTIVE, true).Find(model).Error
	if err != nil {
		return model, nil
	}
	return model, err
}
