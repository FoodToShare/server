package repositories

import (
	"gitlab.com/FoodToShare/server/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type UsersRepository interface {
	Save(*models.User) (*models.User, error)
	Delete(*models.User) (*models.User, error)
	FindAll(*[]models.User) (*[]models.User, error)
	FindByID(*models.User) (*models.User, error)
	FindByEmail(*models.User) (*models.User, error)
}

type usersRepositoryImpl struct {
	db *gorm.DB
}

func NewUsersRepository(db *gorm.DB) *usersRepositoryImpl {
	return &usersRepositoryImpl{db}
}

func (r *usersRepositoryImpl) Delete(model *models.User) (*models.User, error) {
	err := r.db.Model(model).Update("active", "false").Error
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (r *usersRepositoryImpl) Save(model *models.User) (*models.User, error) {
	var totalRecords int64
	var err error
	r.db.Model(&models.User{}).Where(ACTIVE, true).Where("id = ?", model.ID).Count(&totalRecords)
	tx := r.db.Begin()

	if totalRecords == 0 {
		err = tx.Debug().Model(&models.User{}).Create(model).Error
	} else {
		err = tx.Debug().Model(model).Select("*").Updates(model).Error
	}
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	return model, tx.Commit().Error
}

func (r *usersRepositoryImpl) FindAll(dest *[]models.User) (*[]models.User, error) {
	err := r.db.Model(&models.User{}).Preload(clause.Associations).Where(ACTIVE, true).Find(dest).Error
	if err != nil {
		return dest, nil
	}

	return dest, err
}
func (r *usersRepositoryImpl) FindByID(model *models.User) (*models.User, error) {
	err := r.db.Model(&models.User{}).Preload(clause.Associations).Where(ACTIVE, true).Find(model).Error
	if err != nil {
		return model, nil
	}

	return model, err
}

func (r *usersRepositoryImpl) FindByEmail(model *models.User) (*models.User, error) {
	err := r.db.Model(&models.User{}).Preload(clause.Associations).
		Where(ACTIVE, true).Where("email = ?", model.Email).First(model).Error
	if err != nil {
		return model, nil
	}

	return model, err
}
