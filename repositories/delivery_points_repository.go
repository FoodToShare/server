package repositories

import (
	"gitlab.com/FoodToShare/server/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type DeliveryPointsRepository interface {
	Save(*models.DeliveryPoint) (*models.DeliveryPoint, error)
	Delete(*models.DeliveryPoint) (*models.DeliveryPoint, error)
	FindAll(*[]models.DeliveryPoint) (*[]models.DeliveryPoint, error)
	FindByID(*models.DeliveryPoint) (*models.DeliveryPoint, error)
}

type deliveryPointsRepositoryImpl struct {
	db *gorm.DB
}

func NewDeliveryPointRepository(db *gorm.DB) *deliveryPointsRepositoryImpl {
	return &deliveryPointsRepositoryImpl{db}
}
func (r *deliveryPointsRepositoryImpl) Delete(model *models.DeliveryPoint) (*models.DeliveryPoint, error) {
	err := r.db.Model(model).Update("active", "false").Error
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (r *deliveryPointsRepositoryImpl) Save(model *models.DeliveryPoint) (*models.DeliveryPoint, error) {
	var totalRecords int64
	var err error
	r.db.Model(&models.DeliveryPoint{}).Where(ACTIVE, true).Where("id = ?", model.ID).Count(&totalRecords)
	tx := r.db.Begin()

	if totalRecords == 0 {
		err = tx.Debug().Model(&models.DeliveryPoint{}).Create(model).Error
	} else {
		err = tx.Debug().Model(model).Select("*").Updates(model).Error
	}
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	return model, tx.Commit().Error
}

func (r *deliveryPointsRepositoryImpl) FindAll(dest *[]models.DeliveryPoint) (*[]models.DeliveryPoint, error) {
	err := r.db.Model(&models.DeliveryPoint{}).Preload(clause.Associations).Where(ACTIVE, true).Find(dest).Error
	if err != nil {
		return dest, nil
	}
	return dest, err
}

func (r *deliveryPointsRepositoryImpl) FindByID(model *models.DeliveryPoint) (*models.DeliveryPoint, error) {
	err := r.db.Model(&models.DeliveryPoint{}).Preload(clause.Associations).Where(ACTIVE, true).Find(model).Error
	if err != nil {
		return model, nil
	}
	return model, err
}
