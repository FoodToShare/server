package repositories

import (
	"gitlab.com/FoodToShare/server/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type RoutesRepository interface {
	Save(*models.Route) (*models.Route, error)
	Delete(*models.Route) (*models.Route, error)
	FindAll(*[]models.Route) (*[]models.Route, error)
	FindByID(*models.Route) (*models.Route, error)
}

type routesRepositoryImpl struct {
	db *gorm.DB
}

func NewRoutesRepository(db *gorm.DB) *routesRepositoryImpl {
	return &routesRepositoryImpl{db}
}

func (r *routesRepositoryImpl) Save(model *models.Route) (*models.Route, error) {
	var totalRecords int64
	var err error
	r.db.Model(&models.Route{}).Where(ACTIVE, true).Where("id = ?", model.ID).Count(&totalRecords)
	tx := r.db.Begin()

	if totalRecords == 0 {
		err = tx.Debug().Model(&models.Route{}).Create(model).Error
	} else {
		err = tx.Debug().Omit(clause.Associations).Model(model).Select("*").Updates(model).Error
	}
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	return model, tx.Commit().Error
}

func (r *routesRepositoryImpl) FindAll(dest *[]models.Route) (*[]models.Route, error) {
	err := r.db.Model(&models.Route{}).Preload(clause.Associations).Where(ACTIVE, true).Find(dest).Error
	if err != nil {
		return dest, nil
	}

	return dest, err
}

func (r *routesRepositoryImpl) Delete(model *models.Route) (*models.Route, error) {
	err := r.db.Model(model).Update("active", "false").Error
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (r *routesRepositoryImpl) FindByID(model *models.Route) (*models.Route, error) {
	err := r.db.Model(&models.Route{}).Preload(clause.Associations).Where(ACTIVE, true).Find(model).Error
	if err != nil {
		return model, nil
	}

	return model, err
}
