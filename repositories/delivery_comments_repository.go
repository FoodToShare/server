package repositories

import (
	"gitlab.com/FoodToShare/server/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type DeliveryCommentsRepository interface {
	Save(*models.DeliveryComments) (*models.DeliveryComments, error)
	FindAll(*[]models.DeliveryComments) (*[]models.DeliveryComments, error)
	FindByID(*models.DeliveryComments) (*models.DeliveryComments, error)
}

type deliveryCommentsRepositoryImpl struct {
	db *gorm.DB
}

func NewDeliveryCommentsRepository(db *gorm.DB) *deliveryCommentsRepositoryImpl {
	return &deliveryCommentsRepositoryImpl{db}
}

func (r *deliveryCommentsRepositoryImpl) Save(model *models.DeliveryComments) (*models.DeliveryComments, error) {
	var totalRecords int64
	var err error
	r.db.Model(&models.DeliveryComments{}).Where(ACTIVE, true).Where("id = ?", model.ID).Count(&totalRecords)
	tx := r.db.Begin()

	if totalRecords == 0 {
		err = tx.Debug().Model(&models.DeliveryComments{}).Create(model).Error
	} else {
		err = tx.Debug().Model(model).Updates(model).Error
	}
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	return model, tx.Commit().Error
}

func (r *deliveryCommentsRepositoryImpl) FindAll(dest *[]models.DeliveryComments) (*[]models.DeliveryComments, error) {
	err := r.db.Model(&models.DeliveryComments{}).Preload(clause.Associations).Where(ACTIVE, true).Find(dest).Error
	if err != nil {
		return dest, nil
	}

	return dest, err
}

func (r *deliveryCommentsRepositoryImpl) FindByID(model *models.DeliveryComments) (*models.DeliveryComments, error) {
	err := r.db.Model(&models.DeliveryComments{}).Preload(clause.Associations).Where(ACTIVE, true).Find(model).Error
	if err != nil {
		return model, nil
	}

	return model, err
}
