package servers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/FoodToShare/server/routes"
	"gitlab.com/FoodToShare/server/services"
	"log"
	"os"
)

func Start() {

	port := os.Getenv("SERVER_PORT")
	if len(port) == 0 {
		port = "8080"
	}
	services.InitRedis()
	router := gin.Default()
	routes.ConfigRoutes(router)
	log.Printf("Server is running at port: %s", port)
	log.Fatal(router.Run(":" + port))
}
