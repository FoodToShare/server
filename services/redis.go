package services

import (
	"os"
	"strconv"

	"github.com/go-redis/redis"
	"gitlab.com/FoodToShare/server/models"
)

var Redis *redis.Client

func InitRedis() {

	dns := os.Getenv("REDIS_DSN")
	redisPassword := os.Getenv("REDIS_PASSWORD")
	redisDB, errAtoi := strconv.Atoi(os.Getenv("REDIS_DB"))

	if len(dns) == 0 {
		dns = "redis:6379"
	}

	if len(redisPassword) == 0 {
		redisPassword = ""
	}

	if errAtoi != nil {
		redisDB = 0
	}

	Redis = redis.NewClient(&redis.Options{
		Addr:     dns,
		Password: redisPassword,
		DB:       redisDB,
	})

	_, err := Redis.Ping().Result()
	if err != nil {
		panic(err)
	}

}

func FetchAuth(authD *models.AccessDetails) (uint64, error) {
	userid, err := Redis.Get(authD.AccessUuid).Result()
	if err != nil {
		return 0, err
	}
	userID, _ := strconv.ParseUint(userid, 10, 64)
	return userID, nil
}

func DeleteAuth(givenUuid string) (int64, error) {
	deleted, err := Redis.Del(givenUuid).Result()
	if err != nil {
		return 0, err
	}
	return deleted, nil
}
