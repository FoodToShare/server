package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/FoodToShare/server/models"
	"io/ioutil"
	"net/http"
	"os"
	"sort"
	"strings"
)

type matrixResponse struct {
	Code         string      `json:"code"`
	Durations    [][]float64 `json:"durations"`
	Destinations []struct {
		Name     string    `json:"name"`
		Location []float64 `json:"location"`
		Distance float64   `json:"distance"`
	} `json:"destinations"`
	Sources []struct {
		Name     string    `json:"name"`
		Location []float64 `json:"location"`
		Distance float64   `json:"distance"`
	} `json:"sources"`
}

type mapBox struct {
	route  *models.Route
	matrix matrixResponse
}

func newMapBox(route *models.Route) (*mapBox, error) {
	mapbox := &mapBox{route: route}
	err := mapbox.getMatrix()
	if err != nil {
		return mapbox, err
	}
	return mapbox, nil
}

const (
	URIMAPBOX    = "https://api.mapbox.com/directions-matrix/v1/mapbox/driving-traffic/"
	SOURCES      = "?sources=0"
	DESTINATIONS = "&destinations="
	TOKEN        = "&access_token="
)

func OrderRouteByDistance(route *models.Route) error {
	if len(route.DeliveryPoints) <= 1 {
		return nil
	}
	mapbox, err := newMapBox(route)
	if err != nil {
		return err
	}
	err = mapbox.orderRoute()
	if err != nil {
		return err
	}
	return nil
}

func (m *mapBox) orderRoute() error {
	mapDurationPoint := make(map[float64]models.DeliveryPoint)
	err := m.getMatrix()
	if err != nil {
		return err
	}
	keys := make([]float64, 0)
	for i := 0; i <= len(m.route.DeliveryPoints)-1; i++ {
		mapDurationPoint[m.matrix.Durations[0][i]] = m.route.DeliveryPoints[i]
		keys = append(keys, m.matrix.Durations[0][i])
	}
	sort.Float64s(keys)
	var orderedDeliveryPoints []models.DeliveryPoint

	for _, k := range keys {
		orderedDeliveryPoints = append(orderedDeliveryPoints, mapDurationPoint[k])
	}
	m.route.DeliveryPoints = orderedDeliveryPoints

	return nil
}

func (m *mapBox) getMatrix() error {
	url, err := m.buildURL()
	if err != nil {
		return err
	}

	client := &http.Client{}
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	request.Header.Add("Accept", "application/json")
	request.Header.Add("Content-Type", "application/json")
	response, err := client.Do(request)
	if err != nil {
		return err
	}
	if response.StatusCode != 200 {
		return errors.New("request error, status code: " + string(rune(response.StatusCode)))
	}
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(bodyBytes, &m.matrix)
	if err != nil {
		return err
	}
	if m.matrix.Code == "NoRoute" {
		return errors.New("there was no route found for the given coordinates")
	}

	return nil
}

func (m *mapBox) getDestinationsNumbers() (string, error) {
	size := len(m.route.DeliveryPoints)
	var builder strings.Builder
	for i := 1; i <= size; i++ {
		_, err := fmt.Fprintf(&builder, "%d;", i)
		if err != nil {
			return "", err
		}
	}
	response := builder.String()
	response = response[:builder.Len()-1]
	return response, nil
}

func (m *mapBox) getCoordinates() (string, error) {
	locationAssociation := os.Getenv("ASSOCIATION_LOCATION")
	if len(locationAssociation) == 0 {
		return "", errors.New("set the binding location in environment variables")
	}
	var builder strings.Builder
	_, err := fmt.Fprintf(&builder, "%s", locationAssociation)
	if err != nil {
		return "", err
	}
	for i := 0; i <= len(m.route.DeliveryPoints)-1; i++ {
		_, err := fmt.Fprintf(&builder, "%s", m.route.DeliveryPoints[i].LatLongToString())
		if err != nil {
			return "", err
		}
	}
	response := builder.String()
	response = response[:builder.Len()-1]
	return response, nil
}

func (m *mapBox) buildURL() (string, error) {
	mapboxToken := os.Getenv("MAPBOX_TOKEN")
	if len(mapboxToken) == 0 {
		return "", errors.New("could not load environment variable with access token")
	}
	destinationNumbers, err := m.getDestinationsNumbers()
	if err != nil {
		return "", err
	}
	coordinates, err := m.getCoordinates()
	if err != nil {
		return "", err
	}
	return URIMAPBOX + coordinates + SOURCES + DESTINATIONS + destinationNumbers + TOKEN + mapboxToken, nil
}
