package auth

import (
	"errors"
	"gitlab.com/FoodToShare/server/databases"
	"gitlab.com/FoodToShare/server/models"
	"gitlab.com/FoodToShare/server/repositories"
	"gitlab.com/FoodToShare/server/services"
	"net/http"
)

func Login(accessKey string, password string) (*models.TokenDetails, error) {
	userRepository := repositories.NewUsersRepository(databases.GetDatabase())

	user, err := userRepository.FindByEmail(&models.User{Email: accessKey})

	if err != nil {
		return nil, errors.New(ERROR_USER_NOT_FOUND)
	}

	if !user.CheckPassword(password) {
		return nil, errors.New(INCORRECT_PASSWORD)
	}

	token, err := CreateToken(user.ID)
	if err != nil {
		return nil, errors.New(ERROR_IN_TOKEN_CREATION)
	}

	saveErr := CreateAuth(user.ID, token)
	if saveErr != nil {
		return nil, errors.New(ERROR_SAVING_TOKEN)
	}

	return token, nil
}

func Logout(r *http.Request) error {
	accessDetails, err := ExtractTokenMetadata(r)
	if err != nil {
		return err
	}
	deleted, delErr := services.DeleteAuth(accessDetails.AccessUuid)
	if delErr != nil || deleted == 0 {
		return err
	}
	return nil
}

func TokenAuthMiddleware(r *http.Request) error {
	err := TokenValid(r)
	if err != nil {
		return err
	}
	token, err := ExtractTokenMetadata(r)
	if err != nil {
		return err
	}
	_, err = services.FetchAuth(token)
	if err != nil {
		return err
	}
	return nil
}
