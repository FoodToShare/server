package databases

import (
	"gitlab.com/FoodToShare/server/databases/config"
	"gitlab.com/FoodToShare/server/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"sync"
	"time"
)

var db *gorm.DB
var lock = &sync.Mutex{}

func startDB() {
	var err error
	db, err = gorm.Open(postgres.Open(config.BuildDSN()), &gorm.Config{})
	if err != nil {
		log.Fatal("Error: ", err)
	}
	migrate()

	settings, _ := db.DB()
	settings.SetConnMaxIdleTime(10)
	settings.SetMaxOpenConns(100)
	settings.SetConnMaxLifetime(time.Hour)
	db.Set("gorm:auto_preload", true)
}

func GetDatabase() *gorm.DB {
	if db == nil {
		lock.Lock()
		defer lock.Unlock()
		if db == nil {
			startDB()
		}
	}
	return db
}

func migrate() {
	err := db.AutoMigrate(&models.User{}, &models.DeliveryComments{}, &models.Route{}, &models.DeliveryPoint{}, &models.Beneficiary{})
	if err != nil {
		log.Fatal(err.Error())
	}
}
