package config

import (
	"fmt"
	"os"
)

func BuildDSN() string {
	host := os.Getenv("DB_HOST")
	if len(host) == 0 {
		host = "localhost"
	}

	port := os.Getenv("DB_PORT")
	if len(port) == 0 {
		port = "3306"
	}

	user := os.Getenv("DB_USER")
	if len(user) == 0 {
		user = "root"
	}

	dbName := os.Getenv("DB_NAME")
	if len(dbName) == 0 {
		dbName = "fts"
	}

	password := os.Getenv("DB_PASSWORD")

	sslMode := os.Getenv("DB_SSL_MODE")
	if len(sslMode) == 0 {
		sslMode = "disable"
	}
	return fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s password=%s", host, port, user, dbName, sslMode, password)
}
